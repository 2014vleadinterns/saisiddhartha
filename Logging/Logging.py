import os
import logging
from logging.handlers import TimedRotatingFileHandler

LOG_FILENAME = 'log/labLog.log'

def setup_logging():
    LOGGER.setLevel(logging.DEBUG)
    myhandler = TimedRotatingFileHandler(
                                LOG_FILENAME, when='midnight', backupCount=5)

    formatter = logging.Formatter(
        '%(asctime)s - %(message)s',
        datefmt='%Y-%m-%d %I:%M:%S %p')
    myhandler.setFormatter(formatter)
    LOGGER.addHandler(myhandler)

if not os.path.isdir("log"):
    os.mkdir("log")
LOGGER = logging.getLogger('labLog')
setup_logging()
LOG_FD = open(LOG_FILENAME, 'w')
