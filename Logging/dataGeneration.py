import Logging
import time
import random
import sys

status = ['running','stopped']

period = int(sys.argv[1])
samples = int(sys.argv[2])

sampling_frequency = period/samples

for i in range(0,samples):
    time.sleep(sampling_frequency)
    logInfo = str(random.uniform(100,1024)) + '  ' + str(random.uniform(100,2048)) + '  ' + str(random.uniform(100,3600)) + '  ' + str(
              status[random.randint(0,1)])
    #logInfo has DiskUsage,RAMUsage,UpTime,LabStatus
    Logging.LOGGER.info(logInfo)
