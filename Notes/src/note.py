import sys
import re
import xml.etree.ElementTree as ET
from xml.dom.minidom import parseString
import pickle
import json

__all__ = ["newnote", "deletenote", "findnote"]

__total__ = []
__title__ = None
__body__ = None
__key__ = None

def newnote(title,body):
    global __total__
    global __title__
    global __body__
    if title == None:
        __title__ = "Untitled"
    else:
        __title__ = title
    if body == None:
        __body__ = "Untitled"
    else:
        __body__ = body
    node = (__title__,__body__)
    __total__.append(node)
   # print __total__

#def deletenote(title):
#    global __title__
#    __title__ = title
#    for __i__ in range(0,len(__total__)): 
#        if __total__[__i__][0] == __title__ :
#            del __total__[__i__]
#    print __total__

#def deletenote(key):
#    global __key__
#    __key__ = key
#    __i__ = 0
#    for __i__ in range(0,len(__total__)):
#        if re.search(__key__,__total__[__i__][0]):
#            del __total__[__i__]
#    print __total__
    

def findnote(key):
    global __total__
    res = []
    print key
    if key == None:
        key = "Untitled"
    for r in __total__:
        if re.search(key,r[0]):
            res.append(r)
    print res

def deletenote(title):
    global __total__
    if title==None:
        title="Untitled"
    for t in __total__:
        if re.search(title,t[0]):
            ele = [t]
            __total__.remove(ele[0])
    print __total__

def save():
    global __total__
    pickle.dump(dict(__total__),open("save.txt","wb"))

def savexml():
    global __total__
    root = ET.Element("Master_Note")
    for x in __total__:
        doc = ET.SubElement(root, "Note")
        field1 = ET.SubElement(doc, "Title")
        field1.text = x[0]
        field2 = ET.SubElement(doc, "Content")
        field2.text = x[1]
    tree = ET.ElementTree(root)
    tree.write("Notes.xml")

def savejson():
    global __total__
    with open('data.txt','w') as outfile:
        json.dump(__total__,outfile)
