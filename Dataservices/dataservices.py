import sys
import tornado.ioloop
import tornado.web
from tornado.options import define, options,parse_command_line
import tornado.autoreload
import os
import json

import data_services

#data_services.getConnection(sys.argv[1])

class DeployedLabsByInstituteHandler(tornado.web.RequestHandler):
    def get(self,inst_id):
	obj = data_services.getDeployedLabsByInstitute(inst_id)
        self.write(json.dumps(obj))


class DeployedLabsHandler(tornado.web.RequestHandler):
    def get(self):
        obj = data_services.getDeployedLabs()
        self.write(json.dumps(obj))


class DeployedLabsByDisciplineHandler(tornado.web.RequestHandler):
    def get(self,disc_id):
        obj = data_services.getDeployedLabsByDiscipline(disc_id)
        self.write(json.dumps(obj))

	
class ParticipatingInstitutesHandler(tornado.web.RequestHandler):
    def get(self):
	obj = data_services.getParticipatingInstitutes()
        self.write(json.dumps(obj))


class AvailableDisciplinesHandler(tornado.web.RequestHandler):
    def get(self):
        obj = data_services.getAvailableDisciplines()
        self.write(json.dumps(obj))


class LabStatsHandler(tornado.web.RequestHandler):
    def get(self,lab_id):
        obj = data_services.getLabStats(lab_id)
        self.write(json.dumps(obj))


class LabSourceControlInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays LabSourceControlInfo")



application = tornado.web.Application([
    (r"/getDeployedLabsByInstitute/([a-z]+)", DeployedLabsByInstituteHandler),
    (r"/getDeployedLabsByDiscipline/([a-z]+)", DeployedLabsByDisciplineHandler),
    (r"/getDeployedLabs", DeployedLabsHandler),
    (r"/getParticipatingInstitutes", ParticipatingInstitutesHandler),
    (r"/getAvailableDisciplines", AvailableDisciplinesHandler),
    (r"/getLabStats/([a-z]{3}[0-9]+)", LabStatsHandler),
    (r"/getLabSourceControlInfo", LabSourceControlInfoHandler),
    
])


if __name__ == "__main__":
    
    application.listen(9888)
    tornado.autoreload.start()
    for dir, _, files in os.walk('Desktop'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    
    tornado.ioloop.IOLoop.instance().start()
