import data_services
import operator
from collections import OrderedDict
import dataservices

tuple_deployedLabs = ('cse05','vscam_lab','iitr','Indian Institute of Roorkee','cse','computer science and engineering',
     'nitrkl.ac.in/cse05','github.com/cse05','simulated','this is the description of the college')
tuple_labStats = ('civ01','vscam_lab',40.0,100.0,22.0,20.0,100.0,10,'nitrkl.ac.in/cse02')



def run_test_dataservices():
    def test_Participating_Institutes():
        list_initial=[]
        list_initial = data_services.getParticipatingInstitutes()
        data_services.addRecord(tuple_deployedLabs,"deployed_labs")
        list_final = data_services.getParticipatingInstitutes()
        list_initial.append({'INST_ID':tuple_deployedLabs[2] , 'INST_NAME':tuple_deployedLabs[3]})
        assert(list_initial.sort() == list_final.sort())   
        data_services.deleteRecord(tuple_deployedLabs,"deployed_labs") 
    

    def test_Available_Disciplines():
        list_initial=[]
        list_initial = data_services.getAvailableDisciplines()
        data_services.addRecord(tuple_deployedLabs,"deployed_labs")
        list_final = data_services.getAvailableDisciplines()
        list_initial.append({'DISC_ID':tuple_deployedLabs[4] , 'DISC_NAME':tuple_deployedLabs[5]})
        assert(list_initial.sort() == list_final.sort())   
        data_services.deleteRecord(tuple_deployedLabs,"deployed_labs") 
     
    def appendDict(attributes,add_tuple):
        add_dict = {}
        for i,j in zip(attributes,range(0,len(add_tuple))):
            add_dict[i] = add_tuple[j]
        return add_dict
    
    def test_Deployed_Labs():
        list_initial=[]
        list_initial = data_services.getDeployedLabs()
        data_services.addRecord(tuple_deployedLabs,"deployed_labs")
        list_final = data_services.getDeployedLabs()
        attributes = data_services.getAttributes("deployed_labs")
        add_dict = appendDict(attributes,tuple_deployedLabs)
        list_initial.append(add_dict)
        assert(sorted(list_initial) == sorted(list_final))   
        data_services.deleteRecord(tuple_deployedLabs,"deployed_labs")   

    

    def test_DeployedLabs_By_Institute():
        list_initial=[]
        list_initial = data_services.getDeployedLabsByInstitute(tuple_deployedLabs[2])
        data_services.addRecord(tuple_deployedLabs,"deployed_labs")
        list_final = data_services.getDeployedLabsByInstitute(tuple_deployedLabs[2])
        attributes = data_services.getAttributes("deployed_labs")
        add_dict = appendDict(attributes,tuple_deployedLabs)
        list_initial.append(add_dict)
        assert(sorted(list_initial) == sorted(list_final))
        data_services.deleteRecord(tuple_deployedLabs,"deployed_labs")

    def test_DeployedLabs_By_Discipline():
        list_initial=[]
        list_initial = data_services.getDeployedLabsByDiscipline(tuple_deployedLabs[4])
        data_services.addRecord(tuple_deployedLabs,"deployed_labs")
        list_final = data_services.getDeployedLabsByDiscipline(tuple_deployedLabs[4])
        attributes = data_services.getAttributes("deployed_labs")
        add_dict = appendDict(attributes,tuple_deployedLabs)
        list_initial.append(add_dict)
        assert(sorted(list_initial) == sorted(list_final))
        data_services.deleteRecord(tuple_deployedLabs,"deployed_labs")

    
    def test_Labstats():
        list_initial=[]
        list_initial = data_services.getLabStats(tuple_labStats[0])
        data_services.addRecord(tuple_labStats,"lab_stats")
        list_final = data_services.getLabStats(tuple_labStats[0])
        attributes = data_services.getAttributes("lab_stats")
        add_dict = appendDict(attributes,tuple_labStats)
        assert(sorted(list_final[0]) == sorted(add_dict))
        data_services.deleteRecord(tuple_labStats,"lab_stats")
    
    
        
    test_Participating_Institutes()
    test_Available_Disciplines()
    test_Deployed_Labs()
    test_DeployedLabs_By_Institute()
    test_DeployedLabs_By_Discipline()
    test_Labstats()
      
    

run_test_dataservices()
