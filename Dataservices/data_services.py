import sys
import sqlalchemy
from sqlalchemy import *
from sqlalchemy import func
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime, Boolean
import MySQLdb
connection = None


def getConnection(password):
    global connection
    try:
        con = 'mysql://root:' + password + '@localhost/dashboard_db'
        engine = create_engine(con)
        connection = engine.connect()
        Session = sessionmaker(bind=engine)
        session = Session()
        engine.dispose()
    except Exception as e:
        print e
          

def processQuery(result):
    final_list = []
    try :
        for query_row in result:
            final_dict = {}
            for key,value in zip(query_row.keys(),query_row.values()):
                final_dict[key] = value
            final_list.append(final_dict)
        return final_list
    except Exception as e:
        print e    


def getDeployedLabsByInstitute(inst_id):
    global connection
    query = text("Select * from deployed_labs where INST_ID = '" + inst_id + "'") 
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict 
        
    except Exception as e:
        print e         




def getDeployedLabs():
    global connection
    query = text("Select * from deployed_labs")
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict 
    
    except Exception as e:
        print e     
        
    




def getDeployedLabsByDiscipline(disc_id):
    global connection
    query = text("Select * from deployed_labs where DISC_ID = '" + disc_id + "'")
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict
    except Exception as e:
        print e 


def getParticipatingInstitutes():
    global connection
    query = text("Select INST_ID, INST_NAME from deployed_labs")
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict
    except Exception as e:
        print e         



def getAvailableDisciplines():
    global connection
    query = text("Select DISC_ID, DISC_NAME from deployed_labs")
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict 
    except Exception as e:
        print e 


def getLabStats(lab_id):
    global connection
    query = text("Select * from lab_stats where LAB_ID = '" + lab_id + "'")
    try:
        result = connection.execute(query)
        final_dict = processQuery(result)
        return final_dict
    except Exception as e:
        print e         


def addRecord(input_list,table_name):
    global connection
    try:
        query = text("Insert into " + table_name + " values " + str(input_list)  )
        connection.execute(query)
    except Exception as e:
        print e 



def deleteRecord(input_list,table_name):
    global connection
    try:
        query = text("delete from " + table_name + " where LAB_ID = '" + input_list[0] + "'")
        connection.execute(query)
    except Exception as e:
        print e 

def getAttributes(table_name):
    global connection
    try:
        query = text("select column_name from information_schema.columns where table_name = '" + table_name +"'" )
        result = connection.execute(query)
        final_list = []
        for i in result:
             final_list.append(i[0])
        return final_list 
    except Exception as e:
        print e 
        
 

getConnection('ja3@wadhwa')
