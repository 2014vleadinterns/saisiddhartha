import sys
import tempfile

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__tmpt__ = None
__tmpb__ = None
__blob__ = None
__limit__ = 10
c = None

def copytext(text):
    global __text__
    global __limit__
    global __tmpt__
    if len(text) < __limit__ :
        __text__ = text
    else:     
        tmp = tempfile.NamedTemporaryFile(dir='/home/siddharth/Desktop/clipboard/tests', delete=False)
        tmp.seek(0)
        tmp.write(text)
        __tmpt__ = tmp
        c = 1
        #tmpt.write(text)
        #__text__ = tmpt.name
        #tmpt.seek(0)
        #tmpt.read()

def copyblob(blob):
    global __blob__
    global __limit__
    global __tmpb__
    if len(blob) < __limit__ :
        __blob__ = blob
    else:
        tmp = tempfile.NamedTemporaryFile(dir='/home/siddharth/Desktop/clipboard/tests', delete=False)
        tmp.seek(0)
        tmp.write(blob)
        __tmpb__ = tmp
        c = 1
        
def gettext():
    global __text__
    global __tmpt__
    #tmp.seek(0)
    if c==1 :
        return __tmpt__.read()
    else:
        return __text__

def getblob():
    global __blob__
    global __tmpb__
    #tmpb.seek(0)
    #tmpb.read()
    if c==1 :
        return __tmpb__.read()
    else:
        return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
