# -*- coding: UTF-8 -*-
import clipboard

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = 'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        #assert(msg == text)

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello world")
        text = clipboard.gettext()
        #assert(text == "hello world")

    def test_copy_text_text():
        clipboard.reset()
        clipboard.copytext("hello")
        if clipboard.gettext() is not None :
            clipboard.copytext("world")
            text = clipboard.gettext()
            assert(text == "world")

    def test_copy_blob():
        clipboard.reset()
        blob=[1,0,1,0,1,1,1,1,1,1,1,1,1,1,1]
        str1 = ''.join(str(e) for e in blob)
        clipboard.copyblob(str1)
       # print str1
        blob = clipboard.getblob()
       # assert(blob == "101011111111111")

    def test_copy_blob_blob():
        clipboard.reset()
        blob1=[1,0,1,0]
        str1 = ''.join(str(e) for e in blob1)
        clipboard.copyblob(str1)
        if clipboard.getblob() is not None :
            blob2=[1,1,1,1]
            str2 = ''.join(str(e) for e in blob2)
            clipboard.copyblob(str2)
            blob = clipboard.getblob()
            # assert(blob == "1111")
    
    def test_copy_text_blob():
        clipboard.reset()
        clipboard.copytext("hello")
        if clipboard.gettext() is not None :
            blob1=[0,0,0,0]
            str1 = ''.join(str(e) for e in blob1)
            clipboard.copyblob(str1)
            text = clipboard.gettext()
            blob = clipboard.getblob()
            assert(text == "hello")
            # assert(blob == [0,0,0,0])

    def test_copy_blob_text():
        clipboard.reset()
        blob1=[1,1,1,1]
        str1 = ''.join(str(e) for e in blob1)
        clipboard.copyblob(str1)
        if clipboard.getblob() is not None :
            clipboard.copytext("world")
            blob = clipboard.getblob()
            text = clipboard.gettext()
        #    assert(blob == [1,1,1,1])
            assert(text == "world")

    def test_copy_text_reset():
        clipboard.reset()
        clipboard.copytext("hello")
        if clipboard.gettext() is not None :
            clipboard.reset()
            text = clipboard.gettext()
            assert(text == None)

    def test_copy_blob_reset():
        clipboard.reset()
        blob1=[1,1,0,1]
        str1 = ''.join(str(e) for e in blob1)
        clipboard.copyblob(str1)
        if clipboard.getblob() is not None :
            clipboard.reset()
            blob = clipboard.getblob()
            assert(blob == None)
    
    def test_copy_text_blob_text():
        clipboard.reset()
        clipboard.copytext("hello")
        if clipboard.gettext() is not None :
            blob1=[1,1,1,1]
            str1 = ''.join(str(e) for e in blob1)
            clipboard.copyblob(str1)
            if clipboard.getblob() is not None :
                clipboard.copytext("world")
                text = clipboard.gettext()
                blob = clipboard.getblob()
                assert(text == "world")
            #    assert(blob == [1,1,1,1])

    def test_copy_text_blob_blob():
        clipboard.reset()
        clipboard.copytext("hello")
        if clipboard.gettext() is not None :
            blob1=[1,1,1,1]
            str1 = ''.join(str(e) for e in blob1)
            clipboard.copyblob(str1)
            if clipboard.getblob() is not None :
                blob2=[0,0,0,0]
                str2 = ''.join(str(e) for e in blob2)
                clipboard.copyblob(str2)
                text = clipboard.gettext()
                blob = clipboard.getblob()
                assert(text == "hello")
            #    assert(blob == [0,0,0,0])

    def test_copy_text_text_blob():
        clipboard.reset()
        clipboard.copytext("hello,")
        if clipboard.gettext() is not None :
            clipboard.copytext(" world!")
            if clipboard.gettext() is not None :
                blob1=[0,1,1,0]
                str1 = ''.join(str(e) for e in blob1)
                clipboard.copyblob(str1)
                text = clipboard.gettext()
                blob = clipboard.getblob()
                assert(text == " world!")
           #     assert(blob == [0,1,1,0])

    def test_copy_blob_blob_text():
        clipboard.reset()
        blob1=[1,1,1,1]
        str1 = ''.join(str(e) for e in blob1)
        clipboard.copyblob(str1)
        if clipboard.gettext() is not None :
            blob2=[0,0,0,0]
            str2 = ''.join(str(e) for e in blob2)
            clipboard.copyblob(str2)
            if clipboard.getblob() is not None :
                clipboard.copytext("hello!")
                text = clipboard.gettext()
                blob = clipboard.getblob()
                assert(text == "hello!")
           #     assert(blob == [0,0,0,0])
        

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    test_copy_hindi_text()
    test_copy_text_text()
    test_copy_blob()
    test_copy_blob_blob()
    test_copy_text_blob()
    test_copy_blob_text()
    test_copy_text_reset()
    test_copy_blob_reset()
    test_copy_text_blob_text()
    test_copy_text_blob_blob()
    test_copy_text_text_blob()
    test_copy_blob_blob_text()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
